# Maintenance

Name: | About:
--- | ---
*README.md* | `Generic details regarding this project.`

Short function to send an export from the EventLog to Email (Incomplete)

Code forked from [James Pearman's EventsToEmail Script](https://github.com/jimmyp85/Maintenance)

- Short function to send an export from the EventLog to Email (Incomplete)
- This function is not completed and is still being written.
- Currently adapting script to make a cmdlet

[@banterboy](https://github.com/BanterBoy) - Luke Leigh - Aug 2018

[@jimmyp85](https://github.com/jimmyp85) - James Pearman - Aug 2018

```powershell
.EXAMPLE
.\New-ReportConfig.ps1 -ComputerName "servername.domain" -EmailTo "recipient@address.com"
-EmailFrom "sender@address.com" -EmailSubject "This is a test Email Config File" -SMTPServer "email.server.com"
-ReportFileName "Report.html" -Date -ConfigFileName "serverConfig.json"
```
